//
//  RideStatusViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 03/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class RideStatusViewController: UIViewController {

    @IBOutlet var viewSuccess: UIView!
     @IBOutlet var btnEndRide: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        Bundle.main.loadNibNamed("readyToRideView", owner: self, options: nil)
        
        
//        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
//           self.viewSuccess.frame = self.view.bounds
//                         self.view.addSubview(self.viewSuccess)
//                         self.view.bringSubviewToFront(self.viewSuccess)
//        }, completion: nil)
        
        
        self.viewSuccess.frame = self.view.bounds
        self.view.addSubview(self.viewSuccess)
        self.view.bringSubviewToFront(self.viewSuccess)
        
        self.viewSuccess.alpha = 0.8
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                   self.viewSuccess.alpha = 1.0
           }, completion: nil)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.viewSuccess.alpha = 0.3
            }) { (value) in
                self.viewSuccess.removeFromSuperview()
            }
        }
        
        btnEndRide.layer.borderColor = UIColor.init(hexString : "#3BCE33")?.cgColor
        btnEndRide.layer.borderWidth = 1
        
        
    }
    

    @IBAction func btnSideMenu(_ sender: Any) {
        openSideMenu(sender: self)
    }
    
    @IBAction func btnPauseRide(_ sender: Any) {
    }
    @IBAction func btnEndRide(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.tripDetailsVC)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
