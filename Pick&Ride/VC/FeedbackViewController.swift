//
//  FeedbackViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 07/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController,UITextViewDelegate {

     @IBOutlet var txtReview: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtReview.text = "Tell us  your experience"
        txtReview.textColor = UIColor.lightGray
        txtReview.textColor = UIColor.lightGray
        txtReview.selectedTextRange = txtReview.textRange(from: txtReview.beginningOfDocument, to: txtReview.beginningOfDocument)
        txtReview.delegate = self
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {

            txtReview.text = "Tell us  your experience"
            txtReview.textColor = UIColor.lightGray

            txtReview.selectedTextRange = textView.textRange(from: txtReview.beginningOfDocument, to: txtReview.beginningOfDocument)
        }

        // Else if the text view's placeholder is showing and the
        // length of the replacement string is greater than 0, set
        // the text color to black then set its text to the
        // replacement string
         else if txtReview.textColor == UIColor.lightGray && !text.isEmpty {
            txtReview.textColor = UIColor.black
            txtReview.text = text
        }

        // For every other case, the text should change with the usual
        // behavior...
        else {
            return true
        }

        // ...otherwise return false since the updates have already
        // been made
        return false
    }

}
