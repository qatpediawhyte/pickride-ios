//
//  ResetPasswordViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnReset(_ sender: UIButton){
        
    }
    
    @IBAction func btnBack(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
