//
//  RideHistoryViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 06/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class RideHistoryViewController: UIViewController {
    
    struct VCConst {
        static let rideCellId = "ride_list_cellId"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
       
       @IBAction func btnHome(_ sender: Any) {
           if let nav =  self.parent as? UINavigationController{
                 if  nav.containsViewController(ofKind: HomeViewController.self){
                     nav.backToViewController(vc:     HomeViewController.self)
                     self.dismiss(animated: true, completion: nil)
               }
             }
       }


}

extension RideHistoryViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: VCConst.rideCellId, for: indexPath) as! RidehistoryListCell
        cell.selectionStyle = .none
        return cell
    }
}
