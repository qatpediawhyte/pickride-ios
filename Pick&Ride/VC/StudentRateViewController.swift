//
//  StudentRateViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 08/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class StudentRateViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtuniversity: UITextField!
    @IBOutlet weak var txtIdCard: UITextField!
    @IBOutlet weak var txtExpDate: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnHome(_ sender: Any) {
        if let nav =  self.parent as? UINavigationController{
            if  nav.containsViewController(ofKind: HomeViewController.self){
                nav.backToViewController(vc:     HomeViewController.self)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
