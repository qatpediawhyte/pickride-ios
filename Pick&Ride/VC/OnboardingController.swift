//
//  OnboardingController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit



class OnboardingController: UIViewController
{
    
    @IBOutlet weak var collectionRef: UICollectionView!
    
    var imageSource:[UIImage] = [UIImage(named: "test_1")!, UIImage(named: "test_2")!, UIImage(named: "test_3")!, UIImage(named: "test_4")!]
    
    
//    var bannerArray  = [Banner]()
    var dataLoaded = false
    @IBOutlet weak var progressRef: UIPageControl!
    
   
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100), execute: {
            self.collectionRef.reloadData()
            
        })
    }
    
    @IBAction func nextAction(_ sender: UIButton)
    {
        let currentIndex = collectionRef.indexPathsForVisibleItems.first
        print("pathsss",collectionRef.indexPathsForVisibleItems)
        if let cIndex = currentIndex
        {
            
            if cIndex.row == imageSource.count - 1
            {
                // INSTANTIATE LOGIN CONTROLLER
                let homeNavigation = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.rootVC)
                homeNavigation?.modalPresentationStyle = .overFullScreen
                self.present(homeNavigation!, animated: true, completion: nil)
            }
            else{

            let nextIndex = IndexPath(item: cIndex.row+1, section: cIndex.section)
            progressRef.currentPage = nextIndex.row
            collectionRef.scrollToItem(at: nextIndex, at: .centeredHorizontally, animated: true)
            }
        }
    }
    

}

extension OnboardingController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return imageSource.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCollectionCell", for: indexPath) as! OnboardingCollectionCell
        
        cell.image.image = imageSource[indexPath.item]
        
//        if bannerArray.count == 0{
//            if dataLoaded == true{
//                cell.image.image = imageSource[indexPath.item]
//                cell.descriptionLabel.text = descriptionSource[indexPath.item]
//            }
//        }else{
//            let image = bannerArray[indexPath.item].banners_image ?? ""
//            let escapedAddress = image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            if escapedAddress != nil{
//                let imagepath =  URL(string: escapedAddress!)
//                cell.image.kf.setImage(with: imagepath)
//            }
//            else{
//                cell.image.image = UIImage(named:"Ellipse 101")
//            }
//          cell.descriptionLabel.text = bannerArray[indexPath.item].banners_desc ?? ""
//        }
        
        cell.layoutIfNeeded()
        
        return cell
    }

    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let currentIndex = collectionRef.indexPathsForVisibleItems.first
        if let cIndex = currentIndex{
//            if cIndex.row == 4
//            {
//                let cell = collectionRef.cellForItem(at: cIndex) as! OnboardingCollectionCell
//
//            }
//            else
//            {
//
//            }
            progressRef.currentPage = cIndex.row
        }
        collectionRef.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        
        let sizeW = self.view.frame.width
        let sizeH = self.view.frame.height
        print("Height",sizeH,"Width",sizeW)
        return CGSize(width: sizeW, height: sizeH)
        
        //        }
        
    }
    
    
}

