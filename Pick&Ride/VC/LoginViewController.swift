//
//  LoginViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnSignUp(_ sender: UIButton){
        let vc = storyboard?.instantiateViewController(identifier: stryBrdIds.signUpVC)
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func btnForGot(_ sender: UIButton){
        let vc = storyboard?.instantiateViewController(identifier: stryBrdIds.resetPassVC)
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func btnSignIn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
