//
//  PricingViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 06/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class PricingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHome(_ sender: Any) {
        if let nav =  self.parent as? UINavigationController{
              if  nav.containsViewController(ofKind: HomeViewController.self){
                  nav.backToViewController(vc:     HomeViewController.self)
                  self.dismiss(animated: true, completion: nil)
            }
          }
    }
    
    @IBAction func btnPickRide(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.homeVC) as! HomeViewController
        vc.isFromPickAndRide = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
