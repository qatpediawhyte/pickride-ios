//
//  WebContentViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 07/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class WebContentViewController: UIViewController {

    @IBOutlet var lblHeading: UILabel!
    var isTerms = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isTerms == true{
            lblHeading.text = "TERMS AND CONDITION"
        }else{
            lblHeading.text = "PRIVACY POLICY"
        }
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
