//
//  HomeViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import GoogleMaps


class HomeViewController: UIViewController {

    @IBOutlet var viewBike: UIView!
    @IBOutlet var viewCycle: UIView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var imgCycle: UIImageView!
    @IBOutlet var imgBike: UIImageView!
    @IBOutlet var btnScanCode: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var viewBikeCycle: UIView!
    @IBOutlet var viewScan: UIView!
    
   
    var isFromPickAndRide = false
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        determineMyCurrentLocation()
        btnScanCode.layer.borderWidth = 1
        btnScanCode.layer.borderColor = UIColor.init(hexString: "#38D430")?.cgColor
        
        if isFromPickAndRide == true{
            viewBikeCycle.isHidden = false
            setGradientBackground(view: viewBikeCycle)
            viewScan.isHidden = true
            btnBack.setImage(UIImage(named: "back_arrow"), for: .normal)
            viewBike.layer.borderWidth = 1
            viewBike.layer.borderColor = UIColor.init(hexString: "#38D430")?.cgColor
        }else{
            setGradientBackground(view: viewScan)
            btnBack.setImage(UIImage(named: "side_menu"), for: .normal)
            viewBikeCycle.isHidden = true
            viewScan.isHidden = false
        }
    }
    func determineMyCurrentLocation() {
        //self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = false
     //   self.mapView.mapType = .satellite
        
        do {
             // Set the map style by passing the URL of the local file.
             if let styleURL = Bundle.main.url(forResource: "style_json", withExtension: "json") {
               mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
             } else {
               NSLog("Unable to find style.json")
             }
           } catch {
             NSLog("One or more of the map styles failed to load. \(error)")
           }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
                  //locationManager.startUpdatingHeading()
        }
    }
       

    @IBAction func btnScanToRide(_ sender: Any) {
    }
    
    @IBAction func btnScanCode(_ sender: Any) {
    }
    
    @IBAction func txtTakeToNear(_ sender: Any) {
    }
    
    @IBAction func btnBike(_ sender: Any) {
        imgCycle.image = UIImage(named: "cycle_green")
        imgBike.image = UIImage(named: "bike_white")
        viewBike.backgroundColor = UIColor.init(hexString: "#38D430")
        viewCycle.layer.borderWidth = 1
        viewCycle.layer.borderColor = UIColor.init(hexString: "#38D430")?.cgColor
        viewCycle.backgroundColor = UIColor.clear
    }
    
    @IBAction func btnCycke(_ sender: Any) {
        imgCycle.image = UIImage(named: "cycle_icon")
        imgBike.image = UIImage(named: "bike_icon")
        viewCycle.backgroundColor = UIColor.init(hexString: "#38D430")
        viewBike.layer.borderWidth = 1
        viewBike.layer.borderColor = UIColor.init(hexString: "#38D430")?.cgColor
        viewBike.backgroundColor = UIColor.clear
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        if isFromPickAndRide == true{
            self.navigationController?.popViewController(animated: true)
        }else{
          openSideMenu(sender: self)
        }
        
    }
    
}

extension HomeViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocation = locations[0] as CLLocation
        let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 13)
        
        self.mapView?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        
        self.locationManager.stopUpdatingLocation()
       
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}
