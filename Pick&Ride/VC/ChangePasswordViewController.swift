//
//  ChangePasswordViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 06/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

     @IBOutlet var txtNewPass: UITextField!
     @IBOutlet var txtConfirmPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangePass(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
