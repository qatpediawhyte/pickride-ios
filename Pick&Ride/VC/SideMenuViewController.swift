//
//  SideMenuViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    struct VCConst {
        static let listCellID = "side_menu_cell_id"
    }
     let itemList = [["name":"ACCOUNT","image":"account"],["name":"PRICING","image":"pricing"],["name":"RIDE HISTORY","image":"ride_history"],["name":"WALLET","image":"wallet"],["name":"NOTIFICATION","image":"notification"],["name":"SETTING","image":"settings"],["name":"REPORT AN ISSUE","image":"report"],["name":"APPLY FOR STUDENT RATES","image":"applyFor"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLogOut(_ sender: Any) {
        let loginNav = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.loginVC)
        loginNav?.modalPresentationStyle = .overFullScreen
        self.present(loginNav!, animated: true, completion: nil)
    }
    
    @IBAction func btnSocial(_ sender: UIButton) {
        if sender.tag == 10{
            guard let url = URL(string: "https://www.facebook.com") else { return }
            UIApplication.shared.open(url)
        }else if sender.tag == 20{
            guard let url = URL(string: "https://twitter.com/home") else { return }
            UIApplication.shared.open(url)
        }else{
            guard let url = URL(string: "https://www.instagram.com") else { return }
            UIApplication.shared.open(url)
        }
    }
}

extension SideMenuViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCConst.listCellID) as! SideMenuListCell
        cell.lblItem.text = itemList[indexPath.row]["name"]
        cell.imgItem.image  = UIImage(named: itemList[indexPath.row]["image"] ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                    if  nav.containsViewController(ofKind: AccountViewController.self){
                        nav.backToViewController(vc:     AccountViewController.self)
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.accountVC)
                         vc?.modalPresentationStyle = .overFullScreen
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
            break
        case 1:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                if  nav.containsViewController(ofKind: PricingViewController.self){
                    nav.backToViewController(vc:     PricingViewController.self)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.pricingVC)
                    vc?.modalPresentationStyle = .overFullScreen
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            break
        case 2:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                if  nav.containsViewController(ofKind: RideHistoryViewController.self){
                    nav.backToViewController(vc:     RideHistoryViewController.self)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.rideHistoryVC)
                    vc?.modalPresentationStyle = .overFullScreen
                    self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
            break
        case 3:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                    if  nav.containsViewController(ofKind: WalletViewController.self){
                        nav.backToViewController(vc:     WalletViewController.self)
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.walletVC)
                         vc?.modalPresentationStyle = .overFullScreen
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
            break
        case 4:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                if  nav.containsViewController(ofKind: NotificationViewController.self){
                    nav.backToViewController(vc:     NotificationViewController.self)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.notifVC)
                    vc?.modalPresentationStyle = .overFullScreen
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            break
        case 5:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                if  nav.containsViewController(ofKind: SettingsViewController.self){
                    nav.backToViewController(vc:     SettingsViewController.self)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.settingVC)
                    vc?.modalPresentationStyle = .overFullScreen
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            break
        case 6:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                if  nav.containsViewController(ofKind: ReportIssueViewController.self){
                    nav.backToViewController(vc:     ReportIssueViewController.self)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.reportVC)
                    vc?.modalPresentationStyle = .overFullScreen
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            break
        case 7:
            if let nav =  self.navigationController?.presentingViewController as? UINavigationController{
                if  nav.containsViewController(ofKind: ReportIssueViewController.self){
                    nav.backToViewController(vc:     ReportIssueViewController.self)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: stryBrdIds.studeRateVC)
                    vc?.modalPresentationStyle = .overFullScreen
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            break
        default:
            print("invalid")
        }
    }
}
