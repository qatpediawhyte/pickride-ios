//
//  SignUpViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    @IBOutlet weak var imgTerms: UIImageView!
    @IBOutlet var txtCode: [UITextField]!
    @IBOutlet var viewVarification: UIView!
    
    var acceptTerms = false
    override func viewDidLoad() {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("VarificationCode", owner: self, options: nil)
    }

    @IBAction func btnAcceptTerms(_ sender: UIButton){
        if acceptTerms == false{
            acceptTerms = true
            imgTerms.image = UIImage(named: "check")
        }else{
            acceptTerms = false
           imgTerms.image = UIImage(systemName: "square")
        }
    }
    
    @IBAction func btnSignUp(_ sender: UIButton){
           viewVarification.frame = self.view.bounds
             self.view.addSubview(viewVarification)
             self.view.bringSubviewToFront(viewVarification)
             self.viewVarification.transform = self.viewVarification.transform.scaledBy(x: 0.001, y: 0.001)
             
             UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
                 self.viewVarification.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
             }, completion: nil)

             for all in txtCode{
                 all.layer.cornerRadius = 5
                 all.layer.masksToBounds = true
                 all.layer.borderWidth = 1
                 all.layer.borderColor =  UIColor.init(hexString: "#38D430")?.cgColor
                 all.addTarget(self, action: #selector(textChanged), for: .editingChanged)
             }
    }
    
    @IBAction func btnResendCode(_ sender: Any) {
        
    }
    
    @IBAction func btnKeepGoing(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: stryBrdIds.rootVC)
        vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
    }
    
    @objc func textChanged(sender: UITextField) {
           if sender.text?.count ?? 0 > 0 {
               let nextField = sender.superview?.viewWithTag(sender.tag + 1) as? UITextField
               nextField?.becomeFirstResponder()
           }
       }
    
    @IBAction func btnSignIn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
}
