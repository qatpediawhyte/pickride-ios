//
//  ReportIssueViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 07/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class ReportIssueViewController: UIViewController, UITextViewDelegate {

      @IBOutlet var txtComment: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtComment.text = "Comment"
        txtComment.textColor = UIColor.lightGray
        txtComment.textColor = UIColor.lightGray
        txtComment.selectedTextRange = txtComment.textRange(from: txtComment.beginningOfDocument, to: txtComment.beginningOfDocument)
        txtComment.delegate = self
    }
    @IBAction func btnActions(_ sender: Any) {
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnHome(_ sender: Any) {
           if let nav =  self.parent as? UINavigationController{
               if  nav.containsViewController(ofKind: HomeViewController.self){
                   nav.backToViewController(vc:     HomeViewController.self)
                   self.dismiss(animated: true, completion: nil)
               }
           }
       }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {

            txtComment.text = "Comment"
            txtComment.textColor = UIColor.lightGray

            txtComment.selectedTextRange = textView.textRange(from: txtComment.beginningOfDocument, to: txtComment.beginningOfDocument)
        }

        // Else if the text view's placeholder is showing and the
        // length of the replacement string is greater than 0, set
        // the text color to black then set its text to the
        // replacement string
         else if txtComment.textColor == UIColor.lightGray && !text.isEmpty {
            txtComment.textColor = UIColor.black
            txtComment.text = text
        }

        // For every other case, the text should change with the usual
        // behavior...
        else {
            return true
        }

        // ...otherwise return false since the updates have already
        // been made
        return false
    }

}
