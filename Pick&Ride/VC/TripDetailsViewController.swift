//
//  TripDetailsViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 03/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import GoogleMaps

class TripDetailsViewController: UIViewController {

    @IBOutlet var viewBG: UIView!
    @IBOutlet var mapView: GMSMapView!
    
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        determineMyCurrentLocation()
    }
    
    func determineMyCurrentLocation() {
        //self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = false
     //   self.mapView.mapType = .satellite
        
        do {
             // Set the map style by passing the URL of the local file.
             if let styleURL = Bundle.main.url(forResource: "style_json", withExtension: "json") {
               mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
             } else {
               NSLog("Unable to find style.json")
             }
           } catch {
             NSLog("One or more of the map styles failed to load. \(error)")
           }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
                  //locationManager.startUpdatingHeading()
        }
    }
    
    @IBAction func btnHome(_ sender: Any) {
              if let nav =  self.parent as? UINavigationController{
                  if  nav.containsViewController(ofKind: HomeViewController.self){
                      nav.backToViewController(vc:     HomeViewController.self)
                      self.dismiss(animated: true, completion: nil)
                  }
              }
          }
    
    @IBAction func btnSideMenu(_ sender: Any) {
           openSideMenu(sender: self)
       }
    
    @IBAction func btnHelp(_ sender: Any) {
         
    }


}

extension TripDetailsViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocation = locations[0] as CLLocation
        let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 13)
        
        self.mapView?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        
        self.locationManager.stopUpdatingLocation()
       
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}
