//
//  SettingsViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 07/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
         
    @IBAction func btnHome(_ sender: Any) {
        if let nav =  self.parent as? UINavigationController{
            if  nav.containsViewController(ofKind: HomeViewController.self){
                nav.backToViewController(vc:     HomeViewController.self)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func viewActions(_ sender: UIButton) {
        switch sender.tag {
        case 10:
            let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.accountVC)
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 20:
            let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.webVC) as! WebContentViewController
            vc.isTerms = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 30:
            let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.webVC) as! WebContentViewController
             vc.isTerms = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 40:
            let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.feedBackVC)
            self.navigationController?.pushViewController(vc!, animated: true)
            break
        default:
            print("invalid")
        }
    }
    
}
