//
//  WalletViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 06/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {

    struct VCConst {
        static let walleListCellId = "wallt_cell_id"
    }
    
    @IBOutlet var amountView: UIView!
    
    var itemsArray = ["10", "20", "50", "100", "150", "200"]
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        amountView.isHidden = true
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
              openSideMenu(sender: self)
          }
       
    @IBAction func btnPlus(_ sender: Any) {
            amountView.isHidden = false
            self.amountView.transform = self.amountView.transform.scaledBy(x: 0.001, y: 0.001)
                        
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
                    self.amountView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
            }, completion: nil)

    }
    
}

extension WalletViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCConst.walleListCellId, for: indexPath) as! WalletColCell
        cell.lblAmount.text = itemsArray[indexPath.item]
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        if indexPath.item == selectedIndex{
            cell.backgroundColor = UIColor.init(hexString: "#3BCE33")
        }else{
           cell.backgroundColor = UIColor.init(hexString: "#EBEBEB")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2-5, height: collectionView.frame.height/2-5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionView.reloadData()
    }
}
