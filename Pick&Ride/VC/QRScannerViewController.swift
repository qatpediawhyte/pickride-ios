//
//  QRScannerViewController.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 03/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class QRScannerViewController: UIViewController {
    
    @IBOutlet var progreesView: MBCircularProgressBarView!
    
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }

    
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                UIView.animate(withDuration: 1, animations: {
                      self.progreesView.value = 100
                }) { (value) in
                    let vc = self.storyboard?.instantiateViewController(identifier: stryBrdIds.rideStatusVC)
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progreesView.value = 0
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !scannerView.isRunning {
            scannerView.startScanning()
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }

    @IBAction func scanButtonAction(_ sender: UIButton) {
        scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        sender.setTitle(buttonTitle, for: .normal)
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        openSideMenu(sender: self)
    }
}


extension QRScannerViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
       // scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        //presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
    }
    
    
    
}

