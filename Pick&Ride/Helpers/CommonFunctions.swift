//
//  CommonFunctions.swift
//  Pick&Ride
//
//  Created by WC_Macmini on 02/07/20.
//  Copyright © 2020 WC_Macmini. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

struct stryBrdIds {
    static let loginVC = "LoginViewController_id"
    static let signUpVC = "SignUpViewController_id"
    static let resetPassVC = "ResetPasswordViewController_id"
    static let sideMEnuId = "sideMenu_stryBrd_id"
    static let rootVC = "root_navigation_stryBrd_id"
    static let onbordingVC = "onBoarding_stryBrd_id"
    static let rideStatusVC = "RideStatusViewController_id"
    static let tripDetailsVC = "TripDetailsViewController_id"
    static let walletVC = "WalletViewController_id"
    static let accountVC = "AccountViewController_id"
    static let pricingVC = "PricingViewController_id"
    static let homeVC = "HomeViewController_id"
    static let notifVC  = "NotificationViewController_id"
    static let rideHistoryVC = "RideHistoryViewController_id"
    static let settingVC = "SettingsViewController_id"
    static let webVC = "WebContentViewController_ID"
    static let feedBackVC = "FeedbackViewController_id"
    static let reportVC = "ReportIssueViewController_id"
    static let studeRateVC = "StudentRateViewController_id"
}

extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        switch chars.count {
        case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
        case 6: chars = ["F","F"] + chars
        case 8: break
        default: return nil
        }
        self.init(red: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
                  green: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
                  blue: .init(strtoul(String(chars[6...7]), nil, 16)) / 255,
                  alpha: .init(strtoul(String(chars[0...1]), nil, 16)) / 255)
    }
}

func setGradientBackground(view:UIView) {
      let gradient = CAGradientLayer()
       gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
       gradient.endPoint = CGPoint(x: 0.5, y: 2.0)
       let whiteColor = UIColor.white
       gradient.colors = [whiteColor.withAlphaComponent(0.0).cgColor, whiteColor.withAlphaComponent(1.0).cgColor, whiteColor.withAlphaComponent(1.0).cgColor]
       gradient.locations = [NSNumber(value: 0.0),NSNumber(value: 0.2),NSNumber(value: 1.0)]
       gradient.frame = view.bounds
       view.layer.mask = gradient
   }

func checkFirstTime() -> Bool{
    let userD = UserDefaults()
    let value = userD.bool(forKey: "isFirstTime")
    return value
}

func changeFirstTime (){
    let userD = UserDefaults()
    userD.set(true, forKey: "isFirstTime")
}

func openSideMenu(sender:UIViewController){
    
    let menuLeftNavigationController = sender.storyboard!.instantiateViewController(withIdentifier: stryBrdIds.sideMEnuId) as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        menuLeftNavigationController.navigationBar.isHidden = true
        SideMenuManager.default.menuAddPanGestureToPresent(toView: sender.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: sender.navigationController!.view, forMenu: .left)
         SideMenuManager.default.menuWidth = sender.view.frame.width - 80
        sender.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
}

extension UINavigationController
{
    /// Given the kind of a (UIViewController subclass),
    /// removes any matching instances from self's
    /// viewControllers array.
    
    func removeAnyViewControllers(ofKind kind: AnyClass)
    {
        self.viewControllers = self.viewControllers.filter { !$0.isKind(of: kind)}
    }
    
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
    func containsViewController(ofKind kind: AnyClass) -> Bool
    {
        return self.viewControllers.contains(where: { $0.isKind(of: kind) })
    }
}
